﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wheel_Project.BusinessLayer.Helpers_Methods.Domain_Helpers;
using Wheel_Project.DatabaseLayer.dbEntities;

namespace Wheel_Project.BusinessLayer.DomainModel_Layer.Cast_Assembly
{
    public class CastDomain : IAggregateRoot
    {
        public Guid? Id { get; set; }
        [Required(ErrorMessage = "Please Provide Alloy Name", AllowEmptyStrings = false)]
        [Display(Name = "Name")]
        public string AlloyName { get; set; }
        public string AlloyDescription { get; set; }
        [Required(ErrorMessage = "Please Provide Alloy Dimensions", AllowEmptyStrings = false)]
        public string AlloyDimension { get; set; }
        public bool? isDeleted { get; set; }
        public DateTime? CreatedOn { get; set; }

        public AlloyList CastDomainToAlloyList(CastDomain model)
        {
            AlloyList alloyList = new AlloyList();
            model.Id = Guid.NewGuid();
            model.CreatedOn = DateTime.Now;
            model.isDeleted = false;
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<CastDomain, AlloyList>();
            });
            alloyList = Mapper.Map<CastDomain, AlloyList>(model, alloyList);
            return alloyList;
        }
        public IEnumerable<CastDomain> CastDomainToAlloyList(List<AlloyList> model)
        {
            List<CastDomain> castDomainList = new List<CastDomain>();
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<AlloyList, CastDomain>();
            });
            model.ForEach(x =>
            {
                CastDomain obj = null;
                obj = Mapper.Map<AlloyList, CastDomain>(x, obj);
                castDomainList.Add(obj);
            });
            return castDomainList;
        }
    }
}
