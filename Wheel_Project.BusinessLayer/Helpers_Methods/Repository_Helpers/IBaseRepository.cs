﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wheel_Project.BusinessLayer.Helpers_Methods.Domain_Helpers;
using Wheel_Project.BusinessLayer.Helpers_Methods.Specification_Helpers;

namespace Wheel_Project.BusinessLayer.Helpers_Methods.Repository_Helpers
{
    public interface IBaseRepository<customEntity>
        where customEntity : IAggregateRoot
    {
        /// <summary>
        /// Method For Finding Single Record By ID
        /// </summary>
        /// <param name="ID">Guid</param>
        /// <returns>Entity</returns>
        customEntity FindByID(Guid? ID);

        /// <summary>
        /// Method for finding Record by Specification
        /// </summary>
        /// <param name="Object">Custom Entity Specification</param>
        /// <returns>Entity</returns>
        customEntity FindOne(ISpecification<customEntity> Object);

        /// <summary>
        /// Method for Finding List of Entity
        /// </summary>
        /// <param name="Object">Specific Entity</param>
        /// <returns>IEumerable<Entity></returns>
        IEnumerable<customEntity> FindAll(ISpecification<customEntity> Object);

        /// <summary>
        /// Method for Adding New Object
        /// </summary>
        /// <param name="ObjectModel">Entity Model</param>
        /// <returns>boolean</returns>
        bool AddObject(customEntity ObjectModel);

        /// <summary>
        /// Method for Removing Object From Entity
        /// </summary>
        /// <param name="ObjectModel">Entity</param>
        /// <returns>boolean</returns>
        bool removeObject(customEntity ObjectModel);
    }
}
