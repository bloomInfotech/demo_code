﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Wheel_Project.BusinessLayer.Helpers_Methods.Specification_Helpers
{
    public interface ISpecification<T>
    {
        Expression<Func<T,bool>> SpecificExpression { get; }
        bool isSatisfiedByExpression(T Object);
    }
}
