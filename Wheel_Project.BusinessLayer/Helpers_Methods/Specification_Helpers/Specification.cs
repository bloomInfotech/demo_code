﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Wheel_Project.BusinessLayer.Helpers_Methods.Specification_Helpers
{
    public abstract class Specification<T> : ISpecification<T>
    {
        //This is Compiled Expression
        private Func<T, bool> _cExpression;

        private Func<T,bool> cExpression
        {
            get
            {
                return this._cExpression ?? (this._cExpression = SpecificExpression.Compile());
            }
        }

        public abstract Expression<Func<T,bool>> SpecificExpression { get; }

        public bool isSatisfiedByExpression(T ObjectModel)
        {
            return this.cExpression(ObjectModel);
        }
    }
}
