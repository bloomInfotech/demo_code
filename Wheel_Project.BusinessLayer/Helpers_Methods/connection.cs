﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wheel_Project.DatabaseLayer.dbEntities;

namespace Wheel_Project.BusinessLayer.Helpers_Methods
{
    public class connection : IDisposable
    {
        #region Variables
        static long hitCounts = 0;
        protected WheelEntities _entity;
        #endregion

        #region Constructor
        public connection()
        {
            this._entity = new WheelEntities();
            ++hitCounts;
        }
        #endregion

        #region Methods
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
