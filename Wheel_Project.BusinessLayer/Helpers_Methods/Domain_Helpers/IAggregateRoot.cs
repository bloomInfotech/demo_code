﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wheel_Project.BusinessLayer.Helpers_Methods.Domain_Helpers
{
    public interface IAggregateRoot
    {
        Guid? Id { get; }
    }
}
