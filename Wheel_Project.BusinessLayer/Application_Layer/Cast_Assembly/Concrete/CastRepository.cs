﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wheel_Project.BusinessLayer.Application_Layer.Cast_Assembly.Abstract;
using Wheel_Project.BusinessLayer.DomainModel_Layer.Cast_Assembly;
using Wheel_Project.BusinessLayer.Helpers_Methods;
using Wheel_Project.BusinessLayer.Helpers_Methods.Repository_Helpers;
using Wheel_Project.BusinessLayer.Helpers_Methods.Specification_Helpers;

namespace Wheel_Project.BusinessLayer.Application_Layer.Cast_Assembly.Concrete
{
    public class CastRepository : connection, ICastRepository
    {
        IEnumerable<CastDomain> ICastRepository.this[bool getAll]
        {
            get
            {
                var records = this._entity.AlloyLists.ToList();
                return new CastDomain().CastDomainToAlloyList(records);
            }
        }

        bool ICastRepository.this[int id]
        {
            get
            {
                return this._entity.Database.Exists();
            }
        }

        bool ICastRepository.saveNewAlloy(CastDomain model, ref string error)
        {
            try
            {
                var alloyModel = new CastDomain().CastDomainToAlloyList(model);
                this._entity.AlloyLists.Add(alloyModel);
                this._entity.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                error = string.Format("Error Message: {0} | Inner Exception: {1}", ex.Message, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }
    }
}
