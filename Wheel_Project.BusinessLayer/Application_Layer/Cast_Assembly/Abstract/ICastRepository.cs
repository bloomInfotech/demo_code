﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wheel_Project.BusinessLayer.DomainModel_Layer.Cast_Assembly;

namespace Wheel_Project.BusinessLayer.Application_Layer.Cast_Assembly.Abstract
{
    public interface ICastRepository
    {
        bool this[int id] { get; }
        bool saveNewAlloy(CastDomain model, ref string error);
        IEnumerable<CastDomain> this[bool getAll] { get; }
    }
}
