﻿$(function () {    
    $('input[type="submit"]').on('click', function (e) {
        var isValid = true;
        $('input[type="text"]').removeBorder();
        $.each($('input.mandatory'), function (index, elem) {
            var flag = $(elem).checkValue();
            if (!flag)
                isValid = false;
        });
        return isValid;
    });
});
$.fn.checkValue = function () {
    if (this.val() == '') {
        this.css('border', '1px solid red');
        return false;
    }
    return true;
}
$.fn.removeBorder = function () {
    this.removeAttr('style');
}