﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wheel_Project.WebUI.NinjectDependency
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        #region Private Variables
        private IKernel _ninject;
        #endregion

        #region Constructor
        public NinjectDependencyResolver()
        {
            this._ninject = new Ninject.StandardKernel();
            this.AddBindings();
        }
        #endregion

        #region Methods
        private void AddBindings()
        {
            //Binding Cast_Assembly Repositories
            this._ninject.Bind<Wheel_Project.BusinessLayer.Application_Layer.Cast_Assembly.Abstract.ICastRepository>().To<Wheel_Project.BusinessLayer.Application_Layer.Cast_Assembly.Concrete.CastRepository>();
        }

        public object GetService(Type serviceType)
        {
            return this._ninject.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this._ninject.GetAll(serviceType);
        }
        #endregion
    }
}