﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wheel_Project.BusinessLayer.Application_Layer.Cast_Assembly.Abstract;
using Wheel_Project.BusinessLayer.DomainModel_Layer.Cast_Assembly;

namespace Wheel_Project.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private ICastRepository _castRepository;
        public HomeController(ICastRepository castRepositoryParam)
        {
            this._castRepository = castRepositoryParam;
        }
        public ActionResult Index()
        {
            //Interaction With Cast Repository
            bool flag = this._castRepository[1];
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(CastDomain model)
        {
            string error = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    //Interaction With Cast Repository
                    bool flag = this._castRepository.saveNewAlloy(model, ref error);
                    if (flag)
                    {
                        ViewBag.Error = null;
                        ViewBag.Message = "1";
                    }
                }
            }
            catch (Exception ex)
            {
                error = string.Format("Error Message: {0} | Inner Exception: {1}", ex.Message, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            if (!string.IsNullOrEmpty(error))
                ViewBag.Error = error;
            return View(model);
        }
        [HttpGet]
        public ActionResult GetAlloyList()
        {
            return View();
        }
        public PartialViewResult _getAlloyList()
        {
            var records = this._castRepository[true];
            //return PartialView("/Views/Home/Shared/_getAlloyList.cshtml", records);
            return PartialView("Shared/_getAlloyList", records);
        }
    }
}