﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Wheel_Project.WebUI.HelperMethods
{
    public class IPNetworking
    {
        public static string GetIPAddress(ref string error)
        {
            string _getIPAddress = string.Empty;
            try
            {
                foreach (var IPAddress in Dns.GetHostAddresses(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString()))
                {
                    if (IPAddress.AddressFamily.ToString() == "InterNetwork")
                    {
                        _getIPAddress = IPAddress.ToString();
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(_getIPAddress))
                    goto _last;

                foreach (var IPAddress in Dns.GetHostAddresses(Dns.GetHostName()))
                {
                    if (IPAddress.AddressFamily.ToString() == "InterNetwork")
                    {
                        _getIPAddress = IPAddress.ToString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                error = string.Format("Error Message: {0} | Inner Exception: {1}", ex.Message, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        _last:
            return _getIPAddress;
        }
    }
}